package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func play(rise, run int) (int, error) {

	// load map from file
	file, err := os.Open("./input")
	if err != nil {
		return 0, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	width := len(lines[0])
	length := len(lines)
	fmt.Printf("Playing on a %d x %d map, with a rise of %d and a run of %d\n",
		width, length, rise, run)

	// player start coordinates
	playerX := 0
	playerY := 0

	// tree counter
	treeCount := 0

	for x := 0; x < length && playerY < length; x++ {

		if string(lines[playerY][playerX]) == "#" {
			treeCount++
		}

		playerX = (playerX + run) % width
		playerY = (playerY + rise)
	}

	return treeCount, nil
}

func main() {

	total := 0
	hits, err := play(1, 1)
	fmt.Printf("Player ran into %d trees.\n", hits)
	total = hits
	hits, err = play(1, 3)
	fmt.Printf("Player ran into %d trees.\n", hits)
	total = total * hits
	hits, err = play(1, 5)
	fmt.Printf("Player ran into %d trees.\n", hits)
	total = total * hits
	hits, err = play(1, 7)
	fmt.Printf("Player ran into %d trees.\n", hits)
	total = total * hits
	hits, err = play(2, 1)
	fmt.Printf("Player ran into %d trees.\n", hits)
	total = total * hits

	fmt.Printf("All trees multiplied together is %d\n", total)

	if err != nil {
		log.Fatal(err)
	}
}
