package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/gookit/validate"
)

type Passport struct {
	Byr int    `validate:"required|int|min:1920|max:2002"`
	Iyr int    `validate:"required|int|min:2010|max:2020"`
	Eyr int    `validate:"required|int|min:2020|max:2030"`
	Hgt string `validate:"required|hgtValidator"`
	Hcl string `validate:"required|hexcolor"`
	Ecl string `validate:"required|eclValidator"`
	Pid int    `validate:"required|int"`
}

// CustomValidator custom validator in the source struct.
func (f Passport) HgtValidator(val string) bool {
	if strings.HasSuffix(val, "cm") {
		cm, _ := strconv.Atoi(strings.Trim(val, "cm"))
		return cm >= 150 && cm <= 193
	}
	in, _ := strconv.Atoi(strings.Trim(val, "in"))
	return in >= 59 && in <= 76
}

// CustomValidator custom validator in the source struct.
func (f Passport) EclValidator(val string) bool {
	return val == "amb" || val == "blu" || val == "brn" || val == "gry" || val == "grn" || val == "hzl" || val == "oth"
}

func main() {
	file, err := os.Open("./input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	valid := 0
	validPass := 0
	pass := Passport{}
	for x := 0; x < len(lines); x++ {
		if len(lines[x]) > 0 {
			pairs := strings.Split(lines[x], " ")
			for i := 0; i < len(pairs); i++ {
				keys := strings.Split(pairs[i], ":")
				switch keys[0] {
				case "byr":
					pass.Byr, _ = strconv.Atoi(keys[1])
				case "iyr":
					pass.Iyr, _ = strconv.Atoi(keys[1])
				case "eyr":
					pass.Eyr, _ = strconv.Atoi(keys[1])
				case "hgt":
					pass.Hgt = keys[1]
				case "hcl":
					pass.Hcl = keys[1]
				case "ecl":
					pass.Ecl = keys[1]
				case "pid":
					if len(keys[1]) == 9 {
						d, err := strconv.Atoi(keys[1])
						if err != nil {
							fmt.Println(err)
							continue
						}
						pass.Pid = d
					}
				}
			}
		} else {
			v := validate.Struct(pass)
			if v.Validate() {
				validPass++
			}
			pass = Passport{}
		}

	}
	if valid > 6 {
		validPass++
	}
	fmt.Printf("Valid passports: %d\n", validPass)
	num, err := strconv.Atoi("#00003")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Test: %d", num)
}
