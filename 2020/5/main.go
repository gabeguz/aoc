package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var fullSeats [952]int64
	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	for x := 0; x < len(lines); x++ {

		rowDirection := lines[x][0:7]
		seatDirection := lines[x][7:10]

		binRow := strings.ReplaceAll(rowDirection, "F", "0")
		binRow = strings.ReplaceAll(binRow, "B", "1")

		binSeat := strings.ReplaceAll(seatDirection, "L", "0")
		binSeat = strings.ReplaceAll(binSeat, "R", "1")

		row, _ := strconv.ParseInt(binRow, 2, 64)
		seat, _ := strconv.ParseInt(binSeat, 2, 64)
		//fmt.Printf("%d Row: %d Seat: %d\n", row*8+seat, row, seat)
		fmt.Printf("%d\n", row*8+seat)
		fullSeats[row*8+seat] = row*8 + seat
	}

	for x := 6; x < len(fullSeats); x++ {
		if fullSeats[x] != int64(x) {
			fmt.Printf("Seat is %d\n", x)
		}
	}
}
