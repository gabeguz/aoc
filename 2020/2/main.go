package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	validPasswordCount := 0
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	var line string

	for {
		line, err = reader.ReadString('\n')
		if err != nil {
			break
		}

		split := strings.Split(line, ":")
		policy := split[0]
		password := strings.TrimSpace(split[1])

		tmp := strings.Split(policy, " ")
		rules := tmp[0]
		char := tmp[1]
		tmp = strings.Split(rules, "-")
		pos1, err := strconv.Atoi(tmp[0])
		if err != nil {
			log.Fatal(err)
		}
		pos2, err := strconv.Atoi(tmp[1])
		if err != nil {
			log.Fatal(err)
		}

		index1 := pos1 -1
		index2 := pos2 -1
	        char1 := string(password[index1])
		char2 := string(password[index2])

		if (char1 != char2) && (char1 == char || char2 == char) {
			fmt.Printf("Password %s matches policy %s\n", password, policy)
			validPasswordCount++
		} else {
			fmt.Printf("Password %s does not match policy %s\n", password,policy)
		}

	}
	fmt.Printf("There are %d valid passowords.", validPasswordCount)
}
