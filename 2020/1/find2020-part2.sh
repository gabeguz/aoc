#!/bin/bash

for firstNum in $(cat input | sort -n);
do
    for secondNum in $(cat input | sort -n);
    do 
        for thirdNum in $(cat input | sort -n);
        do sum=$(( $firstNum + $secondNum + $thirdNum ))
            if [ $sum == 2020 ]
            then
                echo $firstNum + $secondNum + $thirdNum = $sum
                ans=$(( $firstNum * $secondNum * $thirdNum ))
                echo Answer is: $ans
                exit
            fi
        done
    done
done
