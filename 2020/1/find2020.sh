#!/bin/bash

for firstNum in $(cat input);
do
    for secondNum in $(cat input);
    do sum=$(( $firstNum + $secondNum ))
        if [ $sum == 2020 ]
        then
            echo $firstNum + $secondNum == $sum
            ans=$(( $firstNum * $secondNum ))
            echo Answer is: $ans
        fi
    done
done
